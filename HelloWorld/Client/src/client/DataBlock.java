package HelloWorld.Client.src.client;

import javax.net.ssl.SSLEngineResult;
import java.util.ArrayList;
import java.util.Hashtable;

public class DataBlock {
    
    private final static String HEAD = "Header";
    private final static String NULLIFIED_ROW = "0";
    private final static String NULLIFIED_POINTER = "0";
    private Integer freeSpacePointer = 0;
    private ArrayList<Hashtable<String, String>> offsetDirectory;
    private ArrayList<DataRow> dataRows;
    
    public DataBlock() {
        offsetDirectory = new ArrayList<>();
        dataRows = new ArrayList<>();
        freeSpacePointer = this.toString().length();
        System.out.println();
    }
    
	public void updateRow(String id, String first_name, String last_name){
		DataRow dr;
        Integer offSetAdjustment = 0;

		for(DataRow row : dataRows){
			if(row.getId().equals(id)){
                Integer initialRowLength = row.toString().length();
				row.setFirstName(first_name);
				row.setLastName(last_name);
                offSetAdjustment = initialRowLength - row.toString().length() ;
				break;
			}
		}

        adjustOffsetsForUpdatedRow(id, offSetAdjustment);
        freeSpacePointer = this.toString().length();
	}

	public void deleteRow(String id){
		DataRow dr = null;
		for(DataRow row : dataRows){
			if(row.getId().equals(id)){
				dr = row;
				break;
			}
		}
        if(!(dr == null)){
            int offsetAdjustment = dr.toString().length();
            dataRows.remove(dr);

            adjustOffsetsForDeletedRow(id, offsetAdjustment);
        }
        freeSpacePointer = this.toString().length();
	}

    private void adjustOffsetsForUpdatedRow(String id, int offsetAdjustment) {
        Integer pointer = getPointerWithId(id);
        for(Hashtable<String, String> offset : offsetDirectory){
            if(getIntPointer(offset) > pointer){
                Integer adjustedPointer = getIntPointer(offset) - offsetAdjustment;
                offset.put("pointer", adjustedPointer.toString());
            }
        }
    }

    private void adjustOffsetsForDeletedRow(String id, int offsetAdjustment) {
        Integer pointer = getPointerWithId(id);
        for(Hashtable<String, String> offset : offsetDirectory){
            if(offset.get("id").equals(id)){
                offset.put("id", "0");
                offset.put("pointer", "0");
            }
            if(getIntPointer(offset) > pointer){
                Integer adjustedPointer = getIntPointer(offset) - offsetAdjustment;
                offset.put("pointer", adjustedPointer.toString());
            }
        }
    }

    private Integer getIntPointer(Hashtable<String, String> offset) {
        return Integer.parseInt(offset.get("pointer"));
    }

    public void insertRow(String id, String first_name, String last_name){
        DataRow dr = new DataRow(id, first_name, last_name);
        dataRows.add(dr);
        boolean reusedPointerSlot = false;
        for(Hashtable<String, String> offset : offsetDirectory){
            if(offset.get("id").equals("0")) {
                offset.put("id", id);
                offset.put("pointer", freeSpacePointer.toString());
                reusedPointerSlot = true;
                break;
            }
        }
        if(!reusedPointerSlot){
            Hashtable<String, String> newOff = new Hashtable<>();
            newOff.put("id", id);
            newOff.put("pointer", freeSpacePointer.toString());
            offsetDirectory.add(newOff);
            addLengthOfNewPointerToPreviousPointers();
        }
        freeSpacePointer = this.toString().length();
    }
//Header0020680450180030041222005brawn006jordan0030045555004mike003man
	public String[] selectRow(String id) {
        String[] rowInfo = new String[3];
        String searchString = this.toString();
        for(Hashtable<String, String> offset : offsetDirectory) {
            if(matchIdWithinString(id, searchString, offset)) {
                rowInfo = extractFirstAndLastNameFromString(offset, searchString);
            }
        }
        return rowInfo;
    }

    private boolean matchIdWithinString(String id, String searchString, Hashtable<String, String> offset) {
        return getIdFromString(Integer.parseInt(offset.get("pointer")), searchString).equals(id);
    }

    public String getIdFromString(Integer pointer, String searchString){
        pointer += 3; //Column count is first three digits, skip them.
        int lengthOfId = Integer.parseInt(searchString.substring(pointer, pointer + 3));
        pointer += 3; //Skip id length digits
        return searchString.substring(pointer, pointer + lengthOfId);
    }

    private String[] extractFirstAndLastNameFromString(Hashtable<String, String> offset, String searchString){
        String[] firstAndLastName = new String[2];
        Integer pointer = Integer.parseInt(offset.get("pointer"));
        //Column count is first three digits, skip them.
        pointer += 3;
        int lengthOfId = Integer.parseInt(searchString.substring(pointer, pointer + 3));
        //Skip id length digits
        pointer += 3;
        //Skip id as we are only selecting first and last names
        pointer += lengthOfId;
        //read first name length
        int lengthOfFirstName = Integer.parseInt(searchString.substring(pointer, pointer + 3));
        //skip first name length digits
        pointer += 3;
        //store first name
        firstAndLastName[0] = searchString.substring(pointer, pointer + lengthOfFirstName);
        //skip first name
        pointer += lengthOfFirstName;
        //read last name length
        int lengthOfLastName = Integer.parseInt(searchString.substring(pointer, pointer + 3));
        //skip last name length digits
        pointer += 3;
        //store last name
        firstAndLastName[1] = searchString.substring(pointer, pointer + lengthOfLastName);

        return firstAndLastName;
    }

    private int getPointerWithId(String id) {
        int pointer = 0;
        for (Hashtable<String, String> offset : offsetDirectory) {
            if (offset.get("id").equals(id)) {
                pointer = getIntPointer(offset);
            }
        }
        return pointer;
    }

    private void addLengthOfNewPointerToPreviousPointers(){
        for(Hashtable<String, String> offset :offsetDirectory){
            Integer pointer = Integer.parseInt(offset.get("pointer"));
            pointer += 3;
            offset.put("pointer", pointer.toString());
        }
    }

    public String toString(){
        String rowCount = String.format("%1$03d", dataRows.size());
        String freeSpace = String.format("%1$03d", freeSpacePointer);
        String offsets = "";
        String rows = "";

        try{
            for(Hashtable offset : offsetDirectory){
                Integer pointer = Integer.parseInt((String)offset.get("pointer"));
                offsets += String.format("%1$03d", pointer);
            }
            for(DataRow dr : dataRows){
                rows += dr.toString();
            }
        } catch(Exception e) {
            System.out.println(e.toString());
        }
        
        return HEAD + rowCount + freeSpace + offsets + rows;
    }
}
