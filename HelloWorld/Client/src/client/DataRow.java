package HelloWorld.Client.src.client;

public class DataRow {

    private int number_of_columns = 3;
    private String id = "";
    private String firstName = "";
    private String lastName = "";
    
    public DataRow(String id, String first, String last) {
        this.id = id;
        firstName = first;
        lastName = last;
    }
    
    public String toString(){
        
        return String.format("%1$03d", number_of_columns) + 
               String.format("%1$03d", id.length()) +
               id +
               String.format("%1$03d", firstName.length()) +
               firstName + 
               String.format("%1$03d", lastName.length()) +
               lastName;
    }

    public String getId(){
        return id;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setFirstName(String firstName){
        this.firstName = firstName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    
}
